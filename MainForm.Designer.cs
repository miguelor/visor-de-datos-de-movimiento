﻿namespace Visor_Test_Project
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxAccelerationDisplay = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.labelMS = new System.Windows.Forms.Label();
            this.labelVerticalAcceleration = new System.Windows.Forms.Label();
            this.labelHorizontalAcceleration = new System.Windows.Forms.Label();
            this.textBoxDisplayVerAcceleration = new System.Windows.Forms.TextBox();
            this.textBoxDisplayHorAcceleration = new System.Windows.Forms.TextBox();
            this.groupBoxRollPitch = new System.Windows.Forms.GroupBox();
            this.panelDisplayRoll = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.labelPitchDisplay = new System.Windows.Forms.Label();
            this.textBoxPitchDisplay = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelRollDisplay = new System.Windows.Forms.Label();
            this.textBoxRollDisplay = new System.Windows.Forms.TextBox();
            this.groupBoxHeave = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxHeaveDisplay = new System.Windows.Forms.TextBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTotxtFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setCOMOptionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelDisplay = new System.Windows.Forms.ToolStripStatusLabel();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.buttonOnOff = new System.Windows.Forms.Button();
            this.groupBoxAccelerationDisplay.SuspendLayout();
            this.groupBoxRollPitch.SuspendLayout();
            this.groupBoxHeave.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxAccelerationDisplay
            // 
            this.groupBoxAccelerationDisplay.Controls.Add(this.label1);
            this.groupBoxAccelerationDisplay.Controls.Add(this.labelMS);
            this.groupBoxAccelerationDisplay.Controls.Add(this.labelVerticalAcceleration);
            this.groupBoxAccelerationDisplay.Controls.Add(this.labelHorizontalAcceleration);
            this.groupBoxAccelerationDisplay.Controls.Add(this.textBoxDisplayVerAcceleration);
            this.groupBoxAccelerationDisplay.Controls.Add(this.textBoxDisplayHorAcceleration);
            this.groupBoxAccelerationDisplay.Location = new System.Drawing.Point(12, 28);
            this.groupBoxAccelerationDisplay.Name = "groupBoxAccelerationDisplay";
            this.groupBoxAccelerationDisplay.Size = new System.Drawing.Size(598, 135);
            this.groupBoxAccelerationDisplay.TabIndex = 0;
            this.groupBoxAccelerationDisplay.TabStop = false;
            this.groupBoxAccelerationDisplay.Text = "Acceleration";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(111, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "m/s²";
            // 
            // labelMS
            // 
            this.labelMS.AutoSize = true;
            this.labelMS.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMS.Location = new System.Drawing.Point(111, 40);
            this.labelMS.Name = "labelMS";
            this.labelMS.Size = new System.Drawing.Size(47, 16);
            this.labelMS.TabIndex = 4;
            this.labelMS.Text = "m/s²";
            // 
            // labelVerticalAcceleration
            // 
            this.labelVerticalAcceleration.Location = new System.Drawing.Point(164, 79);
            this.labelVerticalAcceleration.Name = "labelVerticalAcceleration";
            this.labelVerticalAcceleration.Size = new System.Drawing.Size(86, 31);
            this.labelVerticalAcceleration.TabIndex = 3;
            this.labelVerticalAcceleration.Text = "Vertical Acceleration";
            // 
            // labelHorizontalAcceleration
            // 
            this.labelHorizontalAcceleration.Location = new System.Drawing.Point(164, 31);
            this.labelHorizontalAcceleration.Name = "labelHorizontalAcceleration";
            this.labelHorizontalAcceleration.Size = new System.Drawing.Size(86, 28);
            this.labelHorizontalAcceleration.TabIndex = 2;
            this.labelHorizontalAcceleration.Text = "Horizontal Acceleration";
            // 
            // textBoxDisplayVerAcceleration
            // 
            this.textBoxDisplayVerAcceleration.Font = new System.Drawing.Font("Pocket Calculator", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDisplayVerAcceleration.Location = new System.Drawing.Point(17, 73);
            this.textBoxDisplayVerAcceleration.Name = "textBoxDisplayVerAcceleration";
            this.textBoxDisplayVerAcceleration.Size = new System.Drawing.Size(88, 39);
            this.textBoxDisplayVerAcceleration.TabIndex = 1;
            this.textBoxDisplayVerAcceleration.Text = "-.--";
            this.textBoxDisplayVerAcceleration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxDisplayHorAcceleration
            // 
            this.textBoxDisplayHorAcceleration.Font = new System.Drawing.Font("Pocket Calculator", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDisplayHorAcceleration.Location = new System.Drawing.Point(17, 31);
            this.textBoxDisplayHorAcceleration.Name = "textBoxDisplayHorAcceleration";
            this.textBoxDisplayHorAcceleration.Size = new System.Drawing.Size(88, 39);
            this.textBoxDisplayHorAcceleration.TabIndex = 0;
            this.textBoxDisplayHorAcceleration.Text = "-.--";
            this.textBoxDisplayHorAcceleration.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBoxRollPitch
            // 
            this.groupBoxRollPitch.Controls.Add(this.panelDisplayRoll);
            this.groupBoxRollPitch.Controls.Add(this.label3);
            this.groupBoxRollPitch.Controls.Add(this.labelPitchDisplay);
            this.groupBoxRollPitch.Controls.Add(this.textBoxPitchDisplay);
            this.groupBoxRollPitch.Controls.Add(this.label2);
            this.groupBoxRollPitch.Controls.Add(this.labelRollDisplay);
            this.groupBoxRollPitch.Controls.Add(this.textBoxRollDisplay);
            this.groupBoxRollPitch.Location = new System.Drawing.Point(12, 169);
            this.groupBoxRollPitch.Name = "groupBoxRollPitch";
            this.groupBoxRollPitch.Size = new System.Drawing.Size(598, 248);
            this.groupBoxRollPitch.TabIndex = 1;
            this.groupBoxRollPitch.TabStop = false;
            this.groupBoxRollPitch.Text = "Roll / Pitch";
            // 
            // panelDisplayRoll
            // 
            this.panelDisplayRoll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.panelDisplayRoll.Location = new System.Drawing.Point(26, 79);
            this.panelDisplayRoll.Name = "panelDisplayRoll";
            this.panelDisplayRoll.Size = new System.Drawing.Size(237, 152);
            this.panelDisplayRoll.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(429, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(15, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "°";
            // 
            // labelPitchDisplay
            // 
            this.labelPitchDisplay.Location = new System.Drawing.Point(466, 37);
            this.labelPitchDisplay.Name = "labelPitchDisplay";
            this.labelPitchDisplay.Size = new System.Drawing.Size(66, 13);
            this.labelPitchDisplay.TabIndex = 9;
            this.labelPitchDisplay.Text = "Pitch";
            // 
            // textBoxPitchDisplay
            // 
            this.textBoxPitchDisplay.Font = new System.Drawing.Font("Pocket Calculator", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPitchDisplay.Location = new System.Drawing.Point(330, 34);
            this.textBoxPitchDisplay.Name = "textBoxPitchDisplay";
            this.textBoxPitchDisplay.Size = new System.Drawing.Size(93, 39);
            this.textBoxPitchDisplay.TabIndex = 8;
            this.textBoxPitchDisplay.Text = "--.--";
            this.textBoxPitchDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(125, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(15, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "°";
            // 
            // labelRollDisplay
            // 
            this.labelRollDisplay.Location = new System.Drawing.Point(162, 36);
            this.labelRollDisplay.Name = "labelRollDisplay";
            this.labelRollDisplay.Size = new System.Drawing.Size(66, 23);
            this.labelRollDisplay.TabIndex = 6;
            this.labelRollDisplay.Text = "Roll";
            // 
            // textBoxRollDisplay
            // 
            this.textBoxRollDisplay.Font = new System.Drawing.Font("Pocket Calculator", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxRollDisplay.Location = new System.Drawing.Point(26, 34);
            this.textBoxRollDisplay.Name = "textBoxRollDisplay";
            this.textBoxRollDisplay.Size = new System.Drawing.Size(93, 39);
            this.textBoxRollDisplay.TabIndex = 5;
            this.textBoxRollDisplay.Text = "--.--";
            this.textBoxRollDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBoxHeave
            // 
            this.groupBoxHeave.Controls.Add(this.label7);
            this.groupBoxHeave.Controls.Add(this.textBoxHeaveDisplay);
            this.groupBoxHeave.Location = new System.Drawing.Point(616, 28);
            this.groupBoxHeave.Name = "groupBoxHeave";
            this.groupBoxHeave.Size = new System.Drawing.Size(156, 135);
            this.groupBoxHeave.TabIndex = 11;
            this.groupBoxHeave.TabStop = false;
            this.groupBoxHeave.Text = "Heave";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(111, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(22, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "m";
            // 
            // textBoxHeaveDisplay
            // 
            this.textBoxHeaveDisplay.Font = new System.Drawing.Font("Pocket Calculator", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxHeaveDisplay.Location = new System.Drawing.Point(12, 34);
            this.textBoxHeaveDisplay.Name = "textBoxHeaveDisplay";
            this.textBoxHeaveDisplay.Size = new System.Drawing.Size(93, 39);
            this.textBoxHeaveDisplay.TabIndex = 5;
            this.textBoxHeaveDisplay.Text = "--.--";
            this.textBoxHeaveDisplay.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // listBox1
            // 
            this.listBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.HorizontalScrollbar = true;
            this.listBox1.Location = new System.Drawing.Point(12, 437);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(760, 95);
            this.listBox1.TabIndex = 12;
            this.listBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBox1_DrawItem);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.optionsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(784, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saveTotxtFileToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // saveTotxtFileToolStripMenuItem
            // 
            this.saveTotxtFileToolStripMenuItem.Name = "saveTotxtFileToolStripMenuItem";
            this.saveTotxtFileToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.saveTotxtFileToolStripMenuItem.Text = "Save as...";
            this.saveTotxtFileToolStripMenuItem.Click += new System.EventHandler(this.saveTotxtFileToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setCOMOptionsToolStripMenuItem});
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.optionsToolStripMenuItem.Text = "Options";
            // 
            // setCOMOptionsToolStripMenuItem
            // 
            this.setCOMOptionsToolStripMenuItem.Name = "setCOMOptionsToolStripMenuItem";
            this.setCOMOptionsToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
            this.setCOMOptionsToolStripMenuItem.Text = "Set COM options";
            this.setCOMOptionsToolStripMenuItem.Click += new System.EventHandler(this.setCOMOptionsToolStripMenuItem_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabelDisplay});
            this.statusStrip1.Location = new System.Drawing.Point(0, 540);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(784, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.SystemColors.Control;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(42, 17);
            this.toolStripStatusLabel1.Text = "Status:";
            // 
            // toolStripStatusLabelDisplay
            // 
            this.toolStripStatusLabelDisplay.Name = "toolStripStatusLabelDisplay";
            this.toolStripStatusLabelDisplay.Size = new System.Drawing.Size(93, 17);
            this.toolStripStatusLabelDisplay.Text = "Unknown Status";
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // buttonOnOff
            // 
            this.buttonOnOff.Location = new System.Drawing.Point(616, 179);
            this.buttonOnOff.Name = "buttonOnOff";
            this.buttonOnOff.Size = new System.Drawing.Size(156, 23);
            this.buttonOnOff.TabIndex = 15;
            this.buttonOnOff.Text = "Stop COM data listening";
            this.buttonOnOff.UseVisualStyleBackColor = true;
            this.buttonOnOff.Click += new System.EventHandler(this.buttonOnOff_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(784, 562);
            this.Controls.Add(this.buttonOnOff);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.groupBoxRollPitch);
            this.Controls.Add(this.groupBoxHeave);
            this.Controls.Add(this.groupBoxAccelerationDisplay);
            this.Controls.Add(this.menuStrip1);
            this.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Data Visor Test Project";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBoxAccelerationDisplay.ResumeLayout(false);
            this.groupBoxAccelerationDisplay.PerformLayout();
            this.groupBoxRollPitch.ResumeLayout(false);
            this.groupBoxRollPitch.PerformLayout();
            this.groupBoxHeave.ResumeLayout(false);
            this.groupBoxHeave.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxAccelerationDisplay;
        private System.Windows.Forms.GroupBox groupBoxRollPitch;
        private System.Windows.Forms.TextBox textBoxDisplayHorAcceleration;
        private System.Windows.Forms.TextBox textBoxDisplayVerAcceleration;
        private System.Windows.Forms.Label labelVerticalAcceleration;
        private System.Windows.Forms.Label labelHorizontalAcceleration;
        private System.Windows.Forms.Label labelMS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelRollDisplay;
        private System.Windows.Forms.TextBox textBoxRollDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label labelPitchDisplay;
        private System.Windows.Forms.TextBox textBoxPitchDisplay;
        private System.Windows.Forms.GroupBox groupBoxHeave;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBoxHeaveDisplay;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTotxtFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setCOMOptionsToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelDisplay;
        private System.Windows.Forms.Button buttonOnOff;
        private System.Windows.Forms.Panel panelDisplayRoll;
        private System.IO.Ports.SerialPort serialPort1;
    }
}

