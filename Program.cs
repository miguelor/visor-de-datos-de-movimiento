﻿/* ---------------------------Data Visor test Project------------------------
 * Author:Miguel Otero
 * September 2013
 * 
 * Requisites:
 * 
 * Program will receive a serial ASCII string that is being sent from an external device and being received 
 * by a computer serial port (RS232).
 * 
 * This data should be presented in the main screen (graphically or/and numerically) and be 
 * possible to save into a txt file.
 * 
 * 
 * 
     TSS1 data String - Characters meaning______________________________________
     1 caratcer incio :
     2-3 Horizontal acceleration
     4-5-6-7 Vertical Acceleration
     8 Espacio
     9 Espacio si positivo, menos si negativo
     10-11-12-13 Heave measurement (variación de las mediciones) en centimetros
     14 Status flag
     15 Espacio si positivo, menos si negativo
     16-17-18-19  Roll
     20 espacio
     21 Espacio si positivo, menos si negativo
     22-23-24-27 Pitch
   ___________________________________________________________________________________
 * 
 * Acceleration is received as ASCII coded Hexadecimal. 
 * The other numerical parameters are received as ASCII coded Decimal.
 * Horizontal acceleration uses units of 3.83 cm/s2  Range:   [0 , 9.81 ] m/s2
 * Vertical acceleration uses units of 0.0625 cm/s2  Range:   [-20.48 , 20.48] m/s2
 * Heave measurement are in cm                       Range:   [-99.99,+99.99] m
 * Roll and pitch are in degrees                     Range:   [-90.99,90.99]º
 * 
 * Positive roll is Port-side up (Babor) Starborad (Estribor) down
 * Positive pitch is bow up(proa), stern (popa) down
 * 
 * --------------------------------------------------------------------
 * */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Visor_Test_Project
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }


    }



} //End namespace
