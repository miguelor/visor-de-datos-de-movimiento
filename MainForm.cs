﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.IO.Ports;
using System.Threading;
using System.Globalization;
using Microsoft.VisualBasic;
using System.Collections;



namespace Visor_Test_Project
{
    public partial class MainForm : Form
    {
        ArrayList dataCache = new ArrayList();


        public MainForm()
        {
            InitializeComponent();        
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            CheckForIllegalCrossThreadCalls = false;
            listBox1.Items.Insert(0, new ListBoxItem(Color.Blue, "Session started on " + DateTime.Now.Date.ToString()));
            activateDataReception();  //Program starts reading data as default      
            

        }



        private void activateDataReception() //Opens the serial Port
        {
            try
            {
               if (serialPort1.IsOpen == false)
                    serialPort1.Open();
                listBox1.Items.Insert(0, new ListBoxItem(Color.Red, "Data reading started."));
            }
            catch (Exception e) {
                MessageBox.Show("Selected COM port doesn't exists.\n\n"+e.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void deactivateDataReception() //Closes the serial Port
        {
            if (serialPort1.IsOpen == true)
                serialPort1.Close();
            listBox1.Items.Insert(0, new ListBoxItem(Color.Red, "Data reading stopped."));
        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string receivedString = serialPort1.ReadExisting();
            processDatafromCOM(receivedString);
        }




        private void processDatafromCOM(String input) 
        {
            //Read variables
            decimal horizontalAcceleration;
            decimal verticalAcceleration;
            decimal heaveMeasurement;
            char statusFlag;
            String status;
            decimal roll;
            decimal pitch;


            //Processing data from input ASCII String to variables

            horizontalAcceleration = 0.01m * 3.83m * (Int32.Parse(input.Substring(1, 2), NumberStyles.HexNumber)); //(convert from cm/s2 to m/s2) *Horizontal Acceleration unit * units amount
            verticalAcceleration = 0.01m * 0.01m * 3.83m * (Int32.Parse(input.Substring(3, 4), NumberStyles.HexNumber)); //adjustment input data *(convert from cm/s2 to m/s2) *Horizontal Acceleration unit * units amount
            heaveMeasurement = 0.01m*Int32.Parse(input.Substring(8, 5));
            roll = 0.01m * Int32.Parse(input.Substring(14, 5));
            pitch = 0.01m * Int32.Parse(input.Substring(20, 5));
            statusFlag = Char.Parse(input.Substring(13, 1));

            //validating the data. If there's some error in the measure (f.ex, Max./Min.) allowed, it corrects it
            horizontalAcceleration=ValidateHorizontalAccelerationMeasure(horizontalAcceleration);
            verticalAcceleration=ValidateVerticalAccelerationMeasure(verticalAcceleration);
            heaveMeasurement = ValidateHeaveMeasure(heaveMeasurement);
            roll = ValidateRollMeasure(roll);
            pitch = ValidatePitchMeasure(pitch);
              

            // Setting Status variable string from status flag
            switch(statusFlag){
                case 'U':
                    status = "Unaided mode - Settled condition";
                    break;
                case 'u':
                    status = "Unaided mode - Settling";
                    break;
                case 'G':
                    status = "GPS Aided mode - Settled condition";
                    break;
                case 'g':
                    status = "GPS Aided mode - Settling";
                    break;
                case 'H':
                    status = "Heading Aided mode - Settled condition";
                    break;
                case 'h':
                    status = "Heading Aided mode - Settling";
                    break;
                case 'F':
                    status = "Full Aided mode - Settled condition";
                    break;
                case 'f':
                    status = "Full Aided mode - Settling";
                    break;
                default:
                    status = "Unknown";
                    break;
                
            }

            // String info for displaying in the log screen
            String info = DateTime.Now.ToString() +" # "+
                "Hor.Acc: " + horizontalAcceleration.ToString("N2") + " m/s2 # " +
                "Ver.Acc: " + verticalAcceleration.ToString("N2") + " m/s2 # " +
                "Heave: " + heaveMeasurement.ToString("N2") + "cm # " +
                "Status: " + statusFlag.ToString() + " # " +
                "Roll: " + roll.ToString("N2") + "° # " +
                "Pitch: " + pitch.ToString("N2")+"°";

            // String infoCache ,different format than info, to make it easier to 
            // read when passing it to a .txt file

            String infoCache =
                "| " + DateTime.Now.ToString() +
                "  | " + horizontalAcceleration.ToString("0.00").PadLeft(10).PadRight(13) +
                "  | " + verticalAcceleration.ToString("00.00").PadLeft(10).PadRight(13) +
                "  | " + heaveMeasurement.ToString("00.00").PadLeft(8) +
                "  | " + statusFlag.ToString().PadLeft(5).PadRight(5) +
                "  | " + roll.ToString("00.00").PadLeft(8) +
                "  | " + pitch.ToString("00.00").PadLeft(8) + "  |";



            listBox1.Items.Insert(0, new ListBoxItem(Color.Black, info)); //Print the data in the log listBox
            dataCache.Add(infoCache);      //saves the received data into a Array used as a cache


            //Print the data, now in the single displays of the form
            textBoxDisplayHorAcceleration.Text = horizontalAcceleration.ToString("N2");
            textBoxDisplayVerAcceleration.Text = verticalAcceleration.ToString("N2");
            textBoxPitchDisplay.Text = pitch.ToString();
            textBoxRollDisplay.Text = roll.ToString();
            textBoxHeaveDisplay.Text = heaveMeasurement.ToString();
            toolStripStatusLabelDisplay.Text = status;

            //print the graphic representations of the data
            printGraphicDisplays(horizontalAcceleration,verticalAcceleration,pitch,roll);

         

        }


        private void saveTotxtFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog Save = new SaveFileDialog();
            System.IO.StreamWriter sWriter = null;

            Save.Filter = "Text (*.txt)|*.txt|All files(*.*)|*.*";
            Save.CheckPathExists = true;
            Save.Title = "Save as...";
            Save.FileName = "DataLog" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day;
            Save.ShowDialog(this);
            try
            {
                //Writting the log file
                sWriter = File.CreateText(Save.FileName);

                sWriter.WriteLine("#######################################################################################################\n\n");
                sWriter.WriteLine("Motion Data Registry, " + DateTime.Now.ToString());
                sWriter.WriteLine("#######################################################################################################\n\n");
                sWriter.WriteLine("|                      |   Horizontal   |    Vertical    |           |        |           |           |");
                sWriter.WriteLine("|   Date        Time   |  Acceleration  |  Acceleration  |   Heave   | Status |    Roll   |   Pitch   |");
                sWriter.WriteLine("|                      |     (m/s2)     |     (m/s2)     |    (m)    |  Flag  |     (°)   |    (°)    |");
                sWriter.WriteLine("######################################################################################################\n\n");


                foreach (String line in dataCache)
                    sWriter.WriteLine(line);

                sWriter.WriteLine("######################################################################################################\n\n");


                sWriter.Flush();
                sWriter.Close();


            }
            catch (Exception) 
            { }
            

        }


        //Unfinished and wrong function for displaying a inclinometer
        public void printGraphicDisplays(Decimal HorizontalAcceleration,Decimal VerticalAcceleration,Decimal Pitch, Decimal Roll) {

            Graphics RollGraphic = panelDisplayRoll.CreateGraphics();

            RollGraphic.Clear(Color.FromArgb(192,192,255));
            Pen pen = new Pen(Color.Black);

            Point p1 = new Point(panelDisplayRoll.Width / 2, 0);
            Point p2 = new Point(panelDisplayRoll.Width / 2, panelDisplayRoll.Height);
            Point p3 = new Point(0, panelDisplayRoll.Height / 2);
            Point p4 = new Point(panelDisplayRoll.Width, panelDisplayRoll.Height / 2);

            //Draw Horizontal and Vertical axis
            RollGraphic.DrawLine(pen, p1, p2);
            RollGraphic.DrawLine(pen, p3, p4);


            //draw Data
            pen = new Pen(Color.Red);

            double angle = (DegreeToRadian(Decimal.ToDouble(180m-Pitch)));
            double angle2 = (DegreeToRadian(Decimal.ToDouble(Pitch)));
            double m = Math.Tan(angle);
            double m2 = Math.Tan(angle2);
            int longitud = panelDisplayRoll.Width/2;


            int x0 = panelDisplayRoll.Width/2; // x Origin
            int y0 = panelDisplayRoll.Height/2; //y Origin
            Point pp0 = new Point(x0, y0); //Coordenadas 0,0


            int x1 = x0 + Convert.ToInt32(Math.Cos(angle)*400);
            int y1 = y0 + Convert.ToInt32(-m * x1); ;

            int x2 = x0 - Convert.ToInt32(Math.Cos(angle) * 400);
            int y2 = y0 + Convert.ToInt32(m2 * x2);

            
            Point pp1 = new Point(x1,y1);
            Point pp2 = new Point(x2,y2);


            RollGraphic.DrawLine(pen, pp0, pp1);
            RollGraphic.DrawLine(pen, pp0,pp2);
           

        }


        private double DegreeToRadian(double angle)
        {
            return Math.PI * angle / 180.0;
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
             if ( MessageBox.Show ( "Are you sure you want to exit?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1 ) == System.Windows.Forms.DialogResult.Yes )
                this.Close();
        }

        private void buttonOnOff_Click(object sender, EventArgs e)
        {
            if(serialPort1.IsOpen==true)
            {
                deactivateDataReception();
                buttonOnOff.Text = "Start COM data listening";
                 }
            else
            {
                activateDataReception();
                buttonOnOff.Text = "Stop COM data listening";
                
            }
        }

        
                //Validation Functions
                private decimal ValidateHorizontalAccelerationMeasure(decimal horizontalAcceleration ){
                    if (horizontalAcceleration < 0)
                        horizontalAcceleration = 0;

                    if (horizontalAcceleration >= 9.81m)
                        horizontalAcceleration = 9.81m;

                    return horizontalAcceleration;
                }

                private decimal ValidateVerticalAccelerationMeasure(decimal verticalAcceleration){
                    if (verticalAcceleration < -20.48m)
                        verticalAcceleration = -20.48m;

                    if (verticalAcceleration > 20.48m)
                        verticalAcceleration = 20.48m;
                    return verticalAcceleration;
                
                }

                private decimal ValidateHeaveMeasure(decimal heaveMeasurement)
                {
                    if (heaveMeasurement < -99.99m)
                        heaveMeasurement = -99.99m;
                    if (heaveMeasurement > 99.99m)
                        heaveMeasurement = 99.99m;
                    return heaveMeasurement;
                }

                private decimal ValidateRollMeasure(decimal roll)
                {
                    if (roll < -90.99m)
                        roll = -90.99m;
                    if (roll > 90.99m)
                        roll = 90.99m;
                    return roll;
                }

                private decimal ValidatePitchMeasure(decimal pitch)
                {
                    if (pitch < -90.99m)
                        pitch = -90.99m;
                    if (pitch > 90.99m)
                        pitch = 90.99m;
                    return pitch;
                }

            


        public class ListBoxItem
        {
            public ListBoxItem(Color c, string m)
            {
                ItemColor = c;
                Message = m;
            }
            public Color ItemColor { get; set; }
            public string Message { get; set; }
        }

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            ListBoxItem item = listBox1.Items[e.Index] as ListBoxItem;
            // Get the current item and cast it to MyListBoxItem

            if (item != null)
            {
                e.Graphics.DrawString( // Draw the appropriate text in the ListBox
                item.Message, // The message linked to the item
                listBox1.Font, // Take the font from the listbox
                new SolidBrush(item.ItemColor), // Set the color 
                0, // X pixel coordinate
                e.Bounds.Top // Y pixel coordinate.  Multiply the index by the ItemHeight defined in the listbox.
                );
            }
            
        }

        private void setCOMOptionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            COMOptionsForm optionsForm = new COMOptionsForm(this);
            optionsForm.Show();
        }




        public void setCOMOptions(String Port,String baudRate,String DataBits,String StopBits,String Parity, String FlowControl) 
        {
            //Test MsgBox to see the parameters
            //MessageBox.Show(Port + " " + baudRate + " " + DataBits + " " + StopBits + " " + Parity + " " + FlowControl);
            

            deactivateDataReception();

            
            
           serialPort1.PortName=Port;
           serialPort1.BaudRate=Convert.ToInt32(baudRate); // Revisar o baudrate para recoller valores válidos. Só traga con 9600
           serialPort1.DataBits=Convert.ToInt32(DataBits);
           
      


           if(StopBits.Equals(1))
               serialPort1.StopBits = System.IO.Ports.StopBits.One;
           if(StopBits.Equals(1.5))
               serialPort1.StopBits = System.IO.Ports.StopBits.OnePointFive;
           if(StopBits.Equals(2))
               serialPort1.StopBits = System.IO.Ports.StopBits.Two;



           if(Parity.Equals("Even"))
               serialPort1.Parity=System.IO.Ports.Parity.Even;
           if(Parity.Equals("Mark"))
               serialPort1.Parity=System.IO.Ports.Parity.Mark;
           if (Parity.Equals("None"))
               serialPort1.Parity=System.IO.Ports.Parity.None;
           if (Parity.Equals("Odd"))
               serialPort1.Parity=System.IO.Ports.Parity.Odd;
           if (Parity.Equals("Space"))
               serialPort1.Parity=System.IO.Ports.Parity.Space;

 
            // flowcontrol?



             activateDataReception();
        }

        
    }
}
