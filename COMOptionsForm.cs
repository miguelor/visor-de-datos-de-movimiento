﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.IO.Ports;

namespace Visor_Test_Project
{
    public partial class COMOptionsForm : Form
    {
        private MainForm mainForm = null;

        public COMOptionsForm()
        {
            InitializeComponent();
        }
       
        public COMOptionsForm(Form callingForm)
        {
            InitializeComponent();
            mainForm = callingForm as MainForm; 
            
        }


        private void COMOptionsForm_Load(object sender, EventArgs e)
        {
            //Default Items

            /*
            comboBox1.Items.Add("COM");
            comboBox1.Items.Add("COM1");
            comboBox1.Items.Add("COM2");
            comboBox1.Items.Add("COM3");
            comboBox1.Items.Add("COM4");*/

            var portNames = SerialPort.GetPortNames();
            foreach (var port in portNames)
            {
              comboBox1.Items.Add(port.ToString());
            }

            comboBox1.Items.Add("<Set custom port...>");
            comboBox1.SelectedIndex = 1;

            int res=300;
            for (int x = 1; x < 14;x++)
            {
                    res = res * 2;
               

                comboBox2.Items.Add(Convert.ToString(res));
            }
            comboBox2.SelectedIndex = 4;


            comboBox3.Items.Add("5");
            comboBox3.Items.Add("6");
            comboBox3.Items.Add("7");
            comboBox3.Items.Add("8");
            comboBox3.SelectedIndex = 3;


            comboBox4.Items.Add("1");
            comboBox4.Items.Add("1.5");
            comboBox4.Items.Add("2");
            comboBox4.SelectedIndex = 0;

            comboBox5.Items.Add("None");
            comboBox5.Items.Add("Odd");
            comboBox5.Items.Add("Even");
            comboBox5.Items.Add("Mark");
            comboBox5.Items.Add("Space");
            comboBox5.SelectedIndex = 0;

            comboBox6.Items.Add("None");
            comboBox6.Items.Add("Hardware");
            comboBox6.Items.Add("Software");
            comboBox6.Items.Add("Custom");
            comboBox6.SelectedIndex = 0;
        }

       

        private void buttonOK_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Applying the changes will stop for a while data reception. Want to proceed?", "Exit", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                String Port;

                //if(comboBox1.SelectedIndex>=0)
                Port = comboBox1.Items[comboBox1.SelectedIndex].ToString();

                String baudRate = comboBox2.Items[comboBox2.SelectedIndex].ToString();
                String DataBits = comboBox3.Items[comboBox3.SelectedIndex].ToString();
                String StopBits = comboBox4.Items[comboBox4.SelectedIndex].ToString();
                String Parity = comboBox5.Items[comboBox5.SelectedIndex].ToString();
                String FlowControl = comboBox6.Items[comboBox6.SelectedIndex].ToString();



                try
                {
                    mainForm.setCOMOptions(Port, baudRate, DataBits, StopBits, Parity, FlowControl);
                }
                catch {MessageBox.Show("Merdacarallo"); }
            }
            this.Close();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            string customOption="";

            if (comboBox1.Items[comboBox1.SelectedIndex].ToString().Equals("<Set custom port...>"))
            {

                customOption = Microsoft.VisualBasic.Interaction.InputBox("Write the name of the COM port", "Select custom port", "", 0, 0);

                if (customOption!="")
                {
                    comboBox1.Items.RemoveAt(comboBox1.Items.Count - 1);
                    comboBox1.Items.Add(customOption);
                    comboBox1.Items.Add("<Set custom port...>");
                   
                    comboBox1.SelectedIndex = comboBox1.Items.Count - 2;
                }
                
            }

            if (comboBox1.SelectedIndex < 0)
                comboBox1.SelectedIndex = 0;


        }


       

        
    }
}
